** Konsep awal: **

- Mereplikasi game aa

*Deadline 1 Februari 2015*

** Draft Konsep lanjutan: **

- Mengembangkan game aa menjadi model simulasi penstabilan elektron pada atom

- Ada beberapa layer disekitar core atom (a.k.a. orbit)

- Atom distabilkan dengan menembakkan elektron ke orbit atom

- Orbit memiliki jumlah: 

	- Elektron yang terisi

	- Elektron yang sisa yang harus diisi

	- Kecepatan orbit

	- Arah orbit

	- Special effect? (Cth: Kalo ngelewatin orbit ini, elektron yang ditembakkan jadi keubah arahnya dikiiiiit)


- Tiap orbit ketika sudah diisi dengan jumlah seharusnya (stabil), ada animasi stabilisasi orbit

- Ketika semua orbit stabil, ada animasi stabilisasi core atom

- Untuk atom yang orbitnya sangat banyak > ~3, setiap 3 orbit yang terstabilkan, orbit terluar menghilang dan digantikan orbit baru dari arah dalam atom yang belum terstabilisasi

** Fitur: **

- Integrasi leaderboard (Google Play leaderboard / IOS game center)

- Endless Mode (Setelah semua level atau suatu jumlah level completed)

- Versi Matematika 

	- Atom center isinya angka yang harus dinolin

	- Elektron orbital jadi angka

	- Elektron yang ditembak isinya operasi matematika

	- Elektron yang ditembakkan harus dikenakan dengan elektron di orbit

- Ensiklopedia (Data tentang atom itu, gimana sejarahnya, konfigurasi eletronnya, simbolnya, penemunya, deskripsi, kegunaan, dkk)

- IAP No Ads + Unlock All Levels instantly + Unlock Endless mode instantly

** Problems: **

- Untuk orbit yang jumlah elektronnya terlalu banyak > 20, bagaimana cara merepresentasikannya?

- Kalau cuma pakai yang maksimum eletron untuk suatu orbit < 20, cuma ada ~60 level. Cukup?

**Strategi Marketing:**

- Promosi diforum:

	- Kaskus

	- Indowebster

	- Reddit

	- Unity showcase forum

- Masukkan app store (Untuk kloningan -> 3 teratas + store gratis):

	- Play store ($25)

	- iOS appstore ($99)

	- Amazon store (free)
 	
	- Windows Phone Store (Rp203.000)

	- Samsung galaxy apps

	- Mobogenie

	- Blackmart

	- Androidpit

** Resource: **

- Konfigurasi eletron per-orbit: http://en.wikipedia.org/wiki/Electron_configurations_of_the_elements_(data_page)