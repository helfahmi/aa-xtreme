﻿using UnityEngine;
using System.Collections;

public class RateScreen : MonoBehaviour {

	public void Show() {
		gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
	}

	private void Hide() {
		gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-1400, 0);
	}

	public void Rate() {
		Hide ();
		#if UNITY_ANDROID
		Application.OpenURL ("market://details?id=com.LastPlay.aa");
		#endif
		#if UNITY_IPHONE
		Application.OpenURL ("itms-apps://itunes.apple.com/app/aa-clash-of-circles/id963505304");
		#endif
		PlayerPrefs.SetInt ("isRate", 1);
	}

	public void Later() {
		Hide ();
	}

	public void NoThanks() {
		Hide ();
		PlayerPrefs.SetInt ("isRate", 1);
	}
}
