﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MuteScript : MonoBehaviour {

	// Use this for initialization
	bool isMute;
	public Sprite enabledSprite;
	public Sprite disabledSprite;
	float defaultVolume;

	void Start () {
		isMute = (PlayerPrefs.GetInt("isMute")==1);
		defaultVolume = AudioListener.volume;
		if (isMute) {
			AudioListener.pause = true;
			gameObject.GetComponent<Image>().sprite = disabledSprite;
		}
		else {
			AudioListener.pause = false;
		}
	}

	public void toggleSound(){
		if (!ChartboostManager.instance.IsShowingAds()) {
			if(isMute){
				enableSound();
			} else {
				disableSound();
			}
		}
	}


	public void disableSound(){
		gameObject.GetComponent<Image>().sprite = disabledSprite;
		isMute = true;
		PlayerPrefs.SetInt ("isMute", 1);
		AudioListener.pause = true;
	}

	public void enableSound(){
		gameObject.GetComponent<Image>().sprite = enabledSprite;
		isMute = false;
		PlayerPrefs.SetInt ("isMute", 0);
		AudioListener.pause = false;
	}
}
