﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class MenuScript : MonoBehaviour {

	public Text currentLevelText;
	public Text currentLevelText2;
	// Use this for initialization
	void Start () {
		int currentLevel = PlayerPrefs.GetInt ("currentLevel");
		int maxLevel = PlayerPrefs.GetInt ("maxLevel");

		if (currentLevel == 0) {
			currentLevel = 1;
		}

		if (maxLevel == 0) {
			maxLevel = 1;
		}

		//Debug.Log ("updating values");
		currentLevelText.GetComponent<Text> ().text = "Level " + currentLevel;
		currentLevelText2.GetComponent<Text>().text = "Your last level: " + maxLevel;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void moveToPlayScene(){
		//Debug.Log ("current level: " + PlayerPrefs.GetInt ("level"));
		if (!ChartboostManager.instance.IsShowingAds()) {
			Application.LoadLevel ("main");
		}
	}

}
