﻿using UnityEngine;
using System.Collections;

public class CoinMenuScript : MonoBehaviour {

	// Use this for initialization
	public GameObject center;
	public int rotationSpeed;
	void Start () {		
	}
	
	// Update is called once per frame
	void Update () {
		transform.RotateAround (center.transform.position, Vector3.forward, Time.deltaTime * rotationSpeed);
	}
}
