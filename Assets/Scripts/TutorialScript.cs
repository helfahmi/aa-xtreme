﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialScript : MonoBehaviour {

	enum State {
		VISIBLE, FADING, INVISIBLE
	};

	State curState;
	const float FADE_TIME = 0.5f;
	const float START_ALPHA = 1f;
	const float END_ALPHA = 0f;
	float i;
	public GameObject tutorial; 
	Image image;
	public Text tutorialText;

	void Awake(){
		image = tutorial.GetComponent<Image> ();
	}

	// Use this for initialization
	void Start () {
		//tutorialText = GetComponentInChildren<Text>();
		curState = State.INVISIBLE;
	}

	public void show(string text){
		curState = State.VISIBLE;
		tutorialText.text = text;
		GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
		image.color = new Color(image.color.r, image.color.g, image.color.b, 1);
		tutorialText.color = new Color(tutorialText.color.r, tutorialText.color.g, tutorialText.color.b, 1);
	}

	public void hide(){
		i = 0f;
		curState = State.FADING;
	}


	public void fadeOut(float start, float end, float length){
		float a = Mathf.Lerp(start, end, i);
		image.color = new Color(image.color.r, image.color.g, image.color.b, a);
		tutorialText.color = new Color(tutorialText.color.r, tutorialText.color.g, tutorialText.color.b, a);

		i += Time.deltaTime * (1 / length);

		if (image.color.a <= 0.01) {
			curState = State.INVISIBLE;
			GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-1500, 0);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (curState == State.FADING) {
			fadeOut(START_ALPHA, END_ALPHA, FADE_TIME);
		}
	}
}
