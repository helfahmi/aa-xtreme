﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InvisibleScript : MonoBehaviour {

	// Use this for initialization

	void Start () {
		hide ();
	}

	public void show(){
		//gameObject.SetActive (true);
		if (!ChartboostManager.instance.IsShowingAds()) {
			gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
		}
	}

	public void hide(){
		//gameObject.SetActive (false);
		gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-1349, 0);
	}
	
	// Update is called once per frame
	void Update () {
		//pos = transform.position;
	}
}
