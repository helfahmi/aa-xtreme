﻿using UnityEngine;
using System.Collections;

public class TakeScreenshot : MonoBehaviour {
	// Update is called once per frame
	#if UNITY_EDITOR
	void LateUpdate () {
		if (Input.GetKeyDown("p")) {
			if (Time.timeScale == 1) {
				Time.timeScale = 0;
			}
			else {
				Time.timeScale = 1;
			}
		}
		if (Input.GetKeyDown("space")) {
			Application.CaptureScreenshot(System.DateTime.Now.ToString("HHmmss")+".png");
			Debug.Log("Screenshot taken");
		}
	}
	#endif
}
