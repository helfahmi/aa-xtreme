﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelectController : MonoBehaviour {

	public GameObject levelButtonPrefab;
	// Use this for initialization
	void Start () {
		int completedLevels = PlayerPrefs.GetInt ("level");
		Debug.Log ("level: " + completedLevels);
		for (int i=1; i <=(completedLevels+1)/2; i++) {
			LevelButtonController levelButton1 = ((GameObject) Instantiate(levelButtonPrefab, new Vector3(-155, 713 - 175 * (i - 1), 0), Quaternion.identity)).GetComponent<LevelButtonController>(); 
			levelButton1.transform.SetParent(this.transform, false);
			levelButton1.setLevelText((i*2 -1).ToString());
			levelButton1.setTimeText(PlayerPrefs.GetInt ((i*2 - 1).ToString()).ToString());
			if(i * 2 <= completedLevels){
				LevelButtonController levelButton2 = ((GameObject) Instantiate(levelButtonPrefab, new Vector3(166, 713 - 175 * (i - 1), 0), Quaternion.identity)).GetComponent<LevelButtonController>();
				levelButton2.transform.SetParent(this.transform, false);
				levelButton2.setLevelText((i*2).ToString());
				levelButton2.setTimeText(PlayerPrefs.GetInt ((i*2).ToString()).ToString());
			}
		} 
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
