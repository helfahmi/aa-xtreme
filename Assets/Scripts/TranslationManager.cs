﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class TranslationManager : MonoBehaviour 
{
	public static TranslationManager instance;
	public string dirPath;
	public Dictionary<string, Dictionary<string, string>> dictionary = new Dictionary<string, Dictionary<string, string>>();

	void Awake()
	{
		if(instance != null)
			GameObject.Destroy(instance);
		else
			instance = this;

		this.readTranslationFile ();

		//SystemLanguage lang = Application.systemLanguage;
	//	Debug.Log (lang.ToString ());

		DontDestroyOnLoad(this);
	}

	void readTranslationFile(){
		List<string> transFiles = new List<string>( Directory.GetFiles(@"" + dirPath) );

		for (int i = 0; i < transFiles.Count; i++){  
			//Remove the file path, leaving only the file name and extension  
			//transFiles[i] = Path.GetFileName( transFiles[i] );  
			//Append each file name to the outputString at a new line  
			//Debug.Log (transFiles[i]);

			string[] langNames = Path.GetFileName(transFiles[i]).Split('.');
			if(langNames.Length == 2){

				Dictionary<string, string> langDic = new Dictionary<string, string>();
				StreamReader input = new StreamReader(transFiles[i]);
				
				while(!input.EndOfStream){
					string line = input.ReadLine();
					string[] items = line.Split (new string[]{"=>"}, System.StringSplitOptions.None);
					langDic.Add(items[0], items[1]);
				}

				dictionary.Add(langNames[0], langDic);
				input.Close();
			}
		}

		/*
		StreamReader input = new StreamReader(filePath);
		
		while(!input.EndOfStream)
		{
			string line = input.ReadLine();
			Debug.Log (line);
		}
		
		input.Close( );  */
	}
}