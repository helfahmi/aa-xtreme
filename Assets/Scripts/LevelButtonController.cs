﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LevelButtonController : MonoBehaviour {

	public Text levelText;
	public Text timeText;
	// Use this for initialization
	void Start () {
	
	}

	public void setLevelText(string level){
		levelText.text = "Level " + level;
	}

	public void setTimeText(string time){
		timeText.text = time + "s";
	}

	// Update is called once per frame
	void Update () {
	
	}
}
