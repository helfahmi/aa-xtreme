﻿using UnityEngine;
using System.Collections;

public class MoreApps : MonoBehaviour {
	public void ShowMoreApps() {
		if (!ChartboostManager.instance.IsShowingAds()) {
			ChartboostManager.instance.ShowMoreApps ();
		}
	}
}
