﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;
#if UNITY_ANDROID
using GooglePlayGames;
#endif

public class SignIn : MonoBehaviour {

	// Use this for initialization
	void Start () {
		#if UNITY_ANDROID
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate();
		#endif
		// authenticate user
		Social.localUser.Authenticate((bool success) => {
			// handle success or failure
		});
	}
}
