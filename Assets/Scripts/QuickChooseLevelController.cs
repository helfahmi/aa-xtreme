﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class QuickChooseLevelController : MonoBehaviour {

	// Use this for initialization
	public bool isShown;
	public InputField levelInput;
	void Start () {
		isShown = false;
		hide ();
	}

	public void toggle(){
		if (isShown) {
			hide ();
		} else {
			show ();
		}
	}

	public void show(){
		isShown = true;
		GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 120);
	}

	public void hide(){
		//reset stuff
		isShown = false;
		GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 900);
	}

	public void jumpToLevel(){
		int level = int.Parse(levelInput.text);
		int maxLevel = PlayerPrefs.GetInt ("maxLevel");
		if (level <= maxLevel && level >= 1) {
			PlayerPrefs.SetInt("currentLevel", level);
			Application.LoadLevel ("main");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
