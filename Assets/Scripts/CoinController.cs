﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CoinController : MonoBehaviour {

	public GameplayController gameplayController;
	private Transform myTransform;

	// properties
//	public float rotationSpeed;
//	public int coinSpeed;
//	public int orbitDistance;
//	public int queueDistance;
//	public int queueSpacing;
	public int coinID;
	public Vector3 centerPos;
	public bool isOrbit;
	public AudioClip collisionSound;
	public bool isCollided;

	private Vector3 destinationPos;
	private float epsilon = 0.00001f;
	private bool inOrbit = false;

	// Use this for initialization
	void Start () {
		myTransform = transform;
		destinationPos = myTransform.position;
		isCollided = false;
	}
	
	// Update is called once per frame
	void Update () {
		if ((!inOrbit)&&(Vector2.Distance (centerPos, myTransform.position) > gameplayController.orbitDistance + epsilon)) {
			#if UNITY_EDITOR
			if (Input.GetMouseButtonDown(0)&&(!ChartboostManager.instance.IsShowingAds())&&(!EventSystem.current.IsPointerOverGameObject())) {
			#elif (UNITY_ANDROID || UNITY_IPHONE)
			if (Input.GetMouseButtonDown(0)&&(!ChartboostManager.instance.IsShowingAds())&&(!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))) {
			#endif
				if (gameplayController.headCoinID==coinID) {
					destinationPos = centerPos + (Vector3.down*gameplayController.orbitDistance);
				}
				else {
					destinationPos += Vector3.up * gameplayController.queueSpacing;
				}
			}
			//Debug.Log("move towards" + destinationPos);
			myTransform.position = Vector3.MoveTowards (myTransform.position, destinationPos, Time.deltaTime * gameplayController.coinSpeed);
		}
		else {
			if (coinID==gameplayController.coinsInQueue) {
				// last coin to enter orbit safely
				gameplayController.Win();
			}
			inOrbit = true;
			myTransform.RotateAround (centerPos, Vector3.forward, Time.deltaTime * gameplayController.rotationSpeed * (360f/136));
		}
	}
	
	void OnTriggerEnter2D (Collider2D col) {
		// collide with other coin
		gameplayController.Lose();
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (!isCollided) {
			audio.PlayOneShot (collisionSound, 0.5f);
			isCollided = true;
			CoinController otherController = coll.gameObject.GetComponent<CoinController>();
			if (otherController!=null) {
				otherController.isCollided = true;
			}
		}
	}

	void OnCollisionExit2D(Collision2D coll) {
		isCollided = false;
		CoinController otherController = coll.gameObject.GetComponent<CoinController>();
		if (otherController!=null) {
			otherController.isCollided = false;
		}
	}
}
