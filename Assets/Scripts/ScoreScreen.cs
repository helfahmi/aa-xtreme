﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
#if UNITY_ANDROID
using GooglePlayGames;
#endif

public class ScoreScreen : MonoBehaviour {

	private Text title;
	private GameObject nextLevel;
	private GameObject restartLevel;

	void Start() {
		title = GameObject.Find ("Title").GetComponent<Text> ();
		nextLevel = GameObject.Find ("NextLevel");
		restartLevel = GameObject.Find ("RestartLevel");
	}

	public void Show(bool suc){
		if (suc) {
			title.text = "SUCCESS!\nNEXT LEVEL";
			nextLevel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3, -26);
			restartLevel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3, -900);
		}
		else {
			title.text = "FAIL!\nRETRY LEVEL";
			nextLevel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3, -900);
			restartLevel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3, -26);
		}

		//score.text = sco.ToString();
		gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
	}

	public void ShowLeaderboard() {
		if (!ChartboostManager.instance.IsShowingAds()) {
			// show leaderboard UI
			#if UNITY_ANDROID
			PlayGamesPlatform.Instance.ShowLeaderboardUI("CgkI1Yzcq8gPEAIQAQ");
			#endif
			#if UNITY_IPHONE
			Social.ShowLeaderboardUI();
			#endif
		}
	}

	public void Continue() {
		if (!ChartboostManager.instance.IsShowingAds()) {
			gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-851, 0);
		}
	}

	public void GoHome() {
		if (!ChartboostManager.instance.IsShowingAds()) {
			Application.LoadLevel ("menu");
		}
	}
}
