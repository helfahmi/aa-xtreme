﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SocialPlatforms;
#if UNITY_ANDROID
using GooglePlayGames;
#endif

public class GameplayController : MonoBehaviour {
	
	private LevelManager levelManager;
	public bool deletePrefs = false;

	private Camera camera;
	private CameraCollider cameraCollider;

	//center & coin vars
	public Vector3 centerPos = Vector3.zero;

	// level independent properties
	public float queueDistance;
	public float queueSpacing;
	public float coinSpeed;

	// level dependent properties
	private int currentLevel;

	public float orbitDistance;
	private float coinScale;

	public int coinsInOrbit;
	public int coinsInQueue;
	private bool clockwiseDirection;
	public float rotationSpeed;
	private float speedChangePeriod;
	private float speedChangeFactor;
	private bool speedChangeMultiply;
	private float directionChangePeriod;
	private float timeLimit;
	private float sizeOrbitChangePeriod;
	private float sizeQueueChangePeriod;
	private bool directionChangeTap;
	private float speedChangeTapFactor;
	private bool addOrbitTap;
	private float addOrbitPeriod;

	// speed change
	private bool isSpeedChange;
	private float timeSpeedChange;

	// direction change
	private bool isDirectionChange;
	private float timeDirectionChange;

	// time limit
	private bool isTimeLimited;

	// size orbit change
	private bool isSizeOrbitChange;
	private float timeSizeOrbitChange;
	private bool sizeOrbitBigger;

	// size queue change
	private bool isSizeQueueChange;
	private float timeSizeQueueChange;
	private bool sizeQueueBigger;

	// speed change tap
	private bool isSpeedChangeTap;
	private bool speedChangeTapFaster;

	// add orbit period
	private bool isAddOrbitPeriod;
	private int orbitAdded;
	private float timeAddOrbit;

	// prefabs and controllers
	public CenterController centerPrefab;
	public CoinController coinPrefab;
	private CenterController centerController;
	private List<CoinController> coinControllers;

	// utilities
	private int i;
	public int headCoinID;
	public Text levelText;
	public Text coinLeftText;

	// win, lose
	public float loseForce;
	private bool losing = false;
	private bool winning = false;
	public Sprite CenterWin;
	public Sprite CenterLose;

	// audio
	public AudioClip electronShot, levelComplete, levelFail;

	// tutorial
	public GameObject tutorialBox;
	public bool[] levelsWithTutorial;
	public string[] levelTutorialText;

	// level background color
	public Color[] levelColors;
	public int curColorIndex;

	// score screen
	private ScoreScreen scoreScreen;

	// rate screen
	private RateScreen rateScreen;

	// time
	public GameObject timeText;

	// debug
	public bool debug = false;
	public int forceLevel;

	// Use this for initialization

	void initTutorial(){
		int levelCount = levelManager.levelSettings.Length + 1;
		//index dipake mulai dari 1
		levelsWithTutorial = new bool[levelCount];
		levelTutorialText = new string[levelCount];

		levelsWithTutorial [1] = true;
		levelTutorialText [1] = "Tap to shoot.";

		levelsWithTutorial [2] = true;
		levelTutorialText [2] = "Good!\nDon't let the circles collide";

		levelsWithTutorial [18] = true;
		levelTutorialText [18] = "New!\nSpeed can change";

		levelsWithTutorial [50] = true;
		levelTutorialText [50] = "Level 50! Good job!";

		levelsWithTutorial [62] = true;
		levelTutorialText [62] = "New!\nTime can be limited!";

		levelsWithTutorial [75] = true;
		levelTutorialText [75] = "Level 75! Awesome!";
	}

	private IEnumerator waitThenRemoveTutorialBox(){
		yield return new WaitForSeconds (1.5f);
		tutorialBox.GetComponent<TutorialScript> ().hide ();
	}
	
	void showTutorialIfExist(int level){
		if (levelsWithTutorial [level] == true) {
			tutorialBox.GetComponent<TutorialScript>().show(levelTutorialText[level]);
			StartCoroutine("waitThenRemoveTutorialBox");
		}
	}

	void initBackgroundColor(int curLevel, int levelInterval){
		const int colorCount = 5;
		//index dipake mulai dari 0
		levelColors = new Color[colorCount];
		levelColors [0] = new Color (255/255f, 237/255f, 197/255f);
		levelColors [1] = new Color (254/255f, 216/255f, 178/255f);
		levelColors [2] = new Color (243/255f, 231/255f, 224/255f);
		levelColors [3] = new Color (234/255f, 234/255f, 234/255f);
		levelColors [4] = new Color (222/255f, 233/255f, 239/255f);
		//levelColors [5] = new Color (204/255f, 238/255f, 255/255f);
		//levelColors [6] = new Color (197/255f, 255/255f, 221/255f);
		//levelColors [7] = new Color (255/255f, 197/255f, 200/255f);
		//levelColors [8] = new Color (213/255f, 255/255f, 197/255f);
		//levelColors [9] = new Color (139/255f, 182/255f, 208/255f);
		//levelColors [10] = new Color (173/255f, 208/255f, 139/255f);

		//init current color
		curColorIndex = ((curLevel-1) / levelInterval) % levelColors.Length;
		//Debug.Log (curColorIndex);
		camera.backgroundColor = levelColors[curColorIndex];			

		//Debug.Log (camera.backgroundColor);
	}

	void Start () {
		if (deletePrefs) {
			PlayerPrefs.DeleteAll ();
		}

		// main camera
		camera = Camera.main;
		cameraCollider = camera.GetComponent<CameraCollider>();
		cameraCollider.DisableColliders ();

		levelManager = gameObject.GetComponent<LevelManager> ();

		// level independent properties
		queueDistance = levelManager.queueDistance;
		queueSpacing = levelManager.queueSpacing;
		coinSpeed = levelManager.coinSpeed;

		// level dependent properties
		currentLevel = PlayerPrefs.GetInt("currentLevel");
		if (currentLevel==0) {
			currentLevel = PlayerPrefs.GetInt("maxLevel");
			if (currentLevel==0) {
				currentLevel = 1;
			}
		}
		if (debug) {
			currentLevel = forceLevel;
			PlayerPrefs.SetInt("currentLevel", currentLevel);
			PlayerPrefs.SetInt("maxLevel", currentLevel);
		}

		clockwiseDirection = levelManager.levelSettings [currentLevel - 1].clockwiseDirection;
		rotationSpeed = levelManager.levelSettings [currentLevel - 1].rotationSpeed * (clockwiseDirection ? -1 : 1);
		speedChangePeriod = levelManager.levelSettings [currentLevel - 1].speedChangePeriod;
		speedChangeFactor = levelManager.levelSettings [currentLevel - 1].speedChangeFactor;
		speedChangeMultiply = levelManager.levelSettings [currentLevel - 1].speedChangeMultiply;
		directionChangePeriod = levelManager.levelSettings [currentLevel - 1].directionChangePeriod;
		timeLimit = levelManager.levelSettings [currentLevel - 1].timeLimit;
		sizeOrbitChangePeriod = levelManager.levelSettings [currentLevel - 1].sizeOrbitChangePeriod;
		sizeQueueChangePeriod = levelManager.levelSettings [currentLevel - 1].sizeQueueChangePeriod;
		directionChangeTap = levelManager.levelSettings [currentLevel - 1].directionChangeTap;
		speedChangeTapFactor = levelManager.levelSettings [currentLevel - 1].speedChangeTapFactor;
		addOrbitTap = levelManager.levelSettings [currentLevel - 1].addOrbitTap;
		addOrbitPeriod = levelManager.levelSettings [currentLevel - 1].addOrbitPeriod;

		orbitDistance = levelManager.levelSettings [currentLevel - 1].orbitDistance;
		coinScale = levelManager.levelSettings [currentLevel - 1].coinScale;
		coinsInOrbit = levelManager.levelSettings [currentLevel - 1].coinsInOrbit;
		coinsInQueue = levelManager.levelSettings [currentLevel - 1].coinsInQueue;

		// create center
		centerPos = transform.position;
		centerController = (CenterController) Instantiate (centerPrefab, centerPos, Quaternion.identity);
		centerController.rotationSpeed = rotationSpeed;
		// create orbit
		coinControllers = new List<CoinController>();
		float stepDegree = 355f / coinsInOrbit;
		float currentDegree = 0;
		coinLeftText.text = coinsInQueue.ToString();
		for (i=0; i<coinsInOrbit; i++) {
			CoinController coinController = (CoinController) Instantiate(coinPrefab, centerController.transform.position+(Quaternion.AngleAxis(currentDegree, Vector3.forward)*(Vector3.right*orbitDistance)), Quaternion.AngleAxis(currentDegree+90, Vector3.forward));
			coinControllers.Add(coinController);
			coinController.transform.localScale *= coinScale;
			coinController.gameplayController = this;
			coinController.coinID = 0;
			coinController.centerPos = centerController.transform.position;
			coinController.isOrbit = true;
			currentDegree+=stepDegree;
		}
		// create queue
		Vector3 currentPos = centerController.transform.position + (Vector3.down * queueDistance);
		for (i=0; i<coinsInQueue; i++) {
			CoinController coinController = (CoinController) Instantiate(coinPrefab, currentPos, Quaternion.identity);
			coinControllers.Add(coinController);
			coinController.transform.localScale *= coinScale;
			coinController.gameplayController = this;
			coinController.coinID = i+1;
			coinController.centerPos = centerController.transform.position;
			coinController.isOrbit = false;
			currentPos+=(Vector3.down * queueSpacing);
		}
		headCoinID = 0;
		
		initTutorial ();
		initBackgroundColor (currentLevel, 5);
		
		levelText.text = "Level " + currentLevel;
		
		// speed change
		isSpeedChange = false;
		if (speedChangePeriod!=0) {
			isSpeedChange = true;
			timeSpeedChange = -100;
		}
		
		// direction change
		isDirectionChange = false;
		if (directionChangePeriod!=0) {
			isDirectionChange = true;
			timeDirectionChange = -100;
		}
		
		// time limit
		isTimeLimited = (timeLimit!=0);

		if (isTimeLimited) {
			timeText.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 424);
		} else {
			timeText.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -900);
		}

		// size orbit change
		isSizeOrbitChange = false;
		if (sizeOrbitChangePeriod!=0) {
			isSizeOrbitChange = true;
			timeSizeOrbitChange = -100;
			sizeOrbitBigger = true;
		}
		
		// size queue change
		isSizeQueueChange = false;
		if (sizeQueueChangePeriod!=0) {
			isSizeQueueChange = true;
			timeSizeQueueChange = -100;
			sizeQueueBigger = true;
		}

		// speed change tap
		isSpeedChangeTap = false;
		if (speedChangeTapFactor!=0) {
			isSpeedChangeTap = true;
			speedChangeTapFaster = true;
		}
		
		// add orbit period
		isAddOrbitPeriod = false;
		if (addOrbitPeriod!=0) {
			isAddOrbitPeriod = true;
			orbitAdded = 0;
			timeAddOrbit = -100;
		}

		// win, lose
		winning = false;
		losing = false;

		// score screen
		scoreScreen = GameObject.Find ("ScoreScreen").GetComponent<ScoreScreen>();

		// rate screen
		rateScreen = GameObject.Find ("RateScreen").GetComponent<RateScreen>();

		showTutorialIfExist (currentLevel);
	}

	void Update() {

		// winning animation
		if (winning) {
			rotationSpeed = Mathf.Lerp(rotationSpeed, 0, Time.deltaTime*2);
		}
		else if (!losing){
			// touch input
			#if UNITY_EDITOR
			if (Input.GetMouseButtonDown(0)&&(!ChartboostManager.instance.IsShowingAds())&&(!EventSystem.current.IsPointerOverGameObject())) {
			#elif (UNITY_ANDROID || UNITY_IPHONE)
			if (Input.GetMouseButtonDown(0)&&(!ChartboostManager.instance.IsShowingAds())&&(!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))) {
				#endif
				if(coinsInQueue - headCoinID >= 0){
					audio.PlayOneShot(electronShot, 0.5f);
				}
				headCoinID++;
				coinLeftText.text = ( (coinsInQueue - headCoinID) >= 0 ? (coinsInQueue - headCoinID) : 0 ).ToString();

				// direction change on tap
				if (directionChangeTap) {
					rotationSpeed *= -1;
				}

				// speed change tap
				if (isSpeedChangeTap) {
					rotationSpeed *= (speedChangeTapFaster ? speedChangeTapFactor : 1/speedChangeTapFactor);
					speedChangeTapFaster = !speedChangeTapFaster;
				}

				// add orbit tap
				if (addOrbitTap) {
					CoinController coinController = (CoinController) Instantiate(coinPrefab, centerController.transform.position+(Quaternion.AngleAxis(90, Vector3.forward)*(Vector3.right*orbitDistance)), Quaternion.AngleAxis(90+90, Vector3.forward));
					coinControllers.Add(coinController);
					coinController.transform.localScale *= coinScale;
					coinController.gameplayController = this;
					coinController.coinID = 0;
					coinController.centerPos = centerController.transform.position;
					coinController.isOrbit = true;
				}
			}
			// speed change
			if (isSpeedChange) {
				if (Time.time-timeSpeedChange > speedChangePeriod) {
					timeSpeedChange = Time.time;
					rotationSpeed *= (speedChangeMultiply ? speedChangeFactor : 1/speedChangeFactor);
					speedChangeMultiply = !speedChangeMultiply;
				}
			}
			// direction change
			if (isDirectionChange) {
				if (Time.time-timeDirectionChange > directionChangePeriod) {
					timeDirectionChange = Time.time;
					rotationSpeed *= -1;
				}
			}
			// time limit
			if (isTimeLimited) {
				timeLimit -= Time.deltaTime;
					if(timeLimit <= 5){
						timeText.transform.GetComponent<Text>().color = Color.red;
					}
					
					timeText.transform.GetComponent<Text>().text = (Mathf.Ceil(timeLimit)).ToString();
				if (timeLimit<=0) {
					timeLimit = 0;
					Lose ();
				}
			}

			// size orbit change
			if (isSizeOrbitChange) {
				if (Time.time-timeSizeOrbitChange > sizeOrbitChangePeriod/3) {
					foreach (CoinController c in coinControllers) {
						if (c.isOrbit) {
							c.transform.localScale *= (sizeOrbitBigger ? 3f/2 : 2f/3);
						}
					}
					timeSizeOrbitChange = Time.time;
					sizeOrbitBigger = !sizeOrbitBigger;
				}
			}

			// size queue change
			if (isSizeQueueChange) {
				if (Time.time-timeSizeQueueChange > sizeQueueChangePeriod/3) {
					foreach (CoinController c in coinControllers) {
						if (!c.isOrbit) {
							c.transform.localScale *= (sizeQueueBigger ? 3f/2 : 2f/3);
						}
					}
					timeSizeQueueChange = Time.time;
					sizeQueueBigger = !sizeQueueBigger;
				}
			}

			// add orbit period
			if (isAddOrbitPeriod) {
				if ((orbitAdded<=15)&&(Time.time-timeAddOrbit>addOrbitPeriod)) {
					CoinController coinController = (CoinController) Instantiate(coinPrefab, centerController.transform.position+(Quaternion.AngleAxis(90, Vector3.forward)*(Vector3.right*orbitDistance)), Quaternion.AngleAxis(90+90, Vector3.forward));
					coinControllers.Add(coinController);
					coinController.transform.localScale *= coinScale;
					coinController.gameplayController = this;
					coinController.coinID = 0;
					coinController.centerPos = centerController.transform.position;
					coinController.isOrbit = true;
					orbitAdded++;
					timeAddOrbit = Time.time;
				}
			}
		}
	}

	public void Win() {
		if ((!winning)&&(!losing)) {
			StartCoroutine ("CoWin");
		}
	}

	private IEnumerator CoWin() {
		winning = true;
		centerController.GetComponent<SpriteRenderer> ().sprite = CenterWin;
		yield return new WaitForSeconds (2f);
		#if UNITY_ANDROID
		Social.ReportScore((long)currentLevel, "CgkI1Yzcq8gPEAIQAQ", (bool success) => {
			// handle success or failure
		});
		#endif
		#if UNITY_IPHONE
			Social.ReportScore((long)currentLevel, "1", (bool success) => {
			// handle success or failure
		});
		#endif
		// calculate level
		currentLevel++;
		if (currentLevel<=(levelManager.levelSettings.Length)) {
			PlayerPrefs.SetInt ("currentLevel", currentLevel);
			if (currentLevel > PlayerPrefs.GetInt("maxLevel")) {
				PlayerPrefs.SetInt("maxLevel", currentLevel);
				if ((currentLevel%8==0)&&(PlayerPrefs.GetInt("isRate")==0)) {
					rateScreen.Show();
				}
			}
			scoreScreen.Show(true);
			ChartboostManager.instance.ShowAds();
			//LoadLevel();
		}
		audio.PlayOneShot (levelComplete, 0.5f);
	}

	public void Lose() {
		if ((!winning)&&(!losing)) {
			StartCoroutine("CoLose");
		}
	}

	private IEnumerator CoLose() {
		losing = true;
		foreach (CoinController c in coinControllers) {
			c.enabled = false;
			c.collider2D.isTrigger = false;
			c.rigidbody2D.isKinematic = false;
			c.rigidbody2D.AddForce(Quaternion.AngleAxis(Random.value*360, Vector3.forward)*(Vector3.right*loseForce));
		}
		audio.PlayOneShot (levelFail, 0.5f);
		centerController.GetComponent<SpriteRenderer> ().sprite = CenterLose;
		cameraCollider.EnableColliders ();
		yield return new WaitForSeconds(1.5f);
		scoreScreen.Show (false);
		ChartboostManager.instance.ShowAds();
		//LoadLevel();
	}

	public void LoadLevel() {
		Destroy (centerController.gameObject);
		foreach (CoinController c in coinControllers) {
			Destroy(c.gameObject);
		}
		Start();
	}
}
