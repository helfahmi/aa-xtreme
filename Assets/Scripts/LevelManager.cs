﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public float queueDistance;
	public float queueSpacing;
	public float coinSpeed;

	public LevelSetting[] levelSettings;

	[System.Serializable]
	public class LevelSetting {
		public float orbitDistance;
		public float coinScale;
		
		public int coinsInQueue;
		public int coinsInOrbit;
		public bool clockwiseDirection;
		public float rotationSpeed;
		public float speedChangePeriod;
		public float speedChangeFactor;
		public bool speedChangeMultiply;
		public float directionChangePeriod;
		public float timeLimit;
		public float sizeOrbitChangePeriod;
		public float sizeQueueChangePeriod;
		public bool directionChangeTap;
		public float speedChangeTapFactor;
		public bool addOrbitTap;
		public float addOrbitPeriod;
	}
}
