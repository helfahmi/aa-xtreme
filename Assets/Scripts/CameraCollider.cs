﻿using UnityEngine;
using System.Collections;

public class CameraCollider : MonoBehaviour {

	Transform northCollider;
	Transform southCollider;
	Transform westCollider;
	Transform eastCollider;

	// Use this for initialization
	void Awake () {
		Transform myTransform = transform;
		northCollider = myTransform.Find ("North Collider");
		northCollider.localPosition = new Vector3 (0, camera.orthographicSize, 0);
		southCollider = myTransform.Find ("South Collider");
		southCollider.localPosition = new Vector3 (0, -camera.orthographicSize, 0);
		westCollider = myTransform.Find ("West Collider");
		westCollider.localPosition = new Vector3 ((-1)*camera.orthographicSize*camera.aspect, 0, 0);
		eastCollider = myTransform.Find ("East Collider");
		eastCollider.localPosition = new Vector3 (camera.orthographicSize*camera.aspect, 0, 0);
	}

	public void DisableColliders() {
		northCollider.gameObject.SetActive (false);
		southCollider.gameObject.SetActive (false);
		westCollider.gameObject.SetActive (false);
		eastCollider.gameObject.SetActive (false);
	}

	public void EnableColliders() {
		northCollider.gameObject.SetActive (true);
		southCollider.gameObject.SetActive (true);
		westCollider.gameObject.SetActive (true);
		eastCollider.gameObject.SetActive (true);
	}
}
